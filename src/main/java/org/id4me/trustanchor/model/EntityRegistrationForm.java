package org.id4me.trustanchor.model;

public class EntityRegistrationForm {
    private String sub;
    private String metadataPolicy;

    public EntityRegistrationForm() {
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getMetadataPolicy() {
        return metadataPolicy;
    }

    public void setMetadataPolicy(String metadataPolicy) {
        this.metadataPolicy = metadataPolicy;
    }
}
