package org.id4me.trustanchor.model;

import net.minidev.json.JSONObject;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;

@Entity
public class FederationEntity {
    @Id
    @GeneratedValue
    private long id;

    @Column(unique = true)
    private String sub;

    private Date iat;
    private Date exp;

    @Lob
    private String jwkSub;

    @Lob
    private HashMap<String, JSONObject> policies;

    public FederationEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public Date getIat() {
        return iat;
    }

    public void setIat(Date iat) {
        this.iat = iat;
    }

    public Date getExp() {
        return exp;
    }

    public void setExp(Date exp) {
        this.exp = exp;
    }

    public String getJwkSub() {
        return jwkSub;
    }

    public void setJwkSub(String jwkSub) {
        this.jwkSub = jwkSub;
    }

    public HashMap<String, JSONObject> getPolicies() {
        return policies;
    }

    public void setPolicies(HashMap<String, JSONObject> policies) {
        this.policies = policies;
    }
}
