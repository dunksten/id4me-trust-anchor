package org.id4me.trustanchor.repository;

import org.id4me.trustanchor.model.FederationEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EntityRepository extends CrudRepository<FederationEntity, Long> {
    FederationEntity getFederationEntityBySub(String sub);
}
