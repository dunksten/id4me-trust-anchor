package org.id4me.trustanchor.service;

import com.nimbusds.jose.jwk.JWKSet;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;

@Service
public class KeyService {
    private JWKSet fedJwkSet;
    private JWKSet jwkSet;

    private KeyService() {
        try {
            //Load keys from file in /resources/KEYS
            this.fedJwkSet =
                    JWKSet.load(
                            new ClassPathResource("KEYS/federationJWKSet.json").getFile()
                    );
            this.jwkSet =
                    JWKSet.load(new ClassPathResource("KEYS/jwkSet.json").getFile());
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
    }

    public JWKSet getPublicFedJwkSet() {
        return this.fedJwkSet.toPublicJWKSet();
    }

    public JWKSet getFedJwkSet() {
        return this.fedJwkSet;
    }

    public JWKSet getPublicJwkSet() {
        return this.jwkSet.toPublicJWKSet();
    }

    public JWKSet getJwkSet() {
        return this.jwkSet;
    }
}
