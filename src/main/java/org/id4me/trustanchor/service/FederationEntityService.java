package org.id4me.trustanchor.service;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jwt.SignedJWT;
import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.openid.connect.sdk.federation.entities.*;
import com.nimbusds.openid.connect.sdk.federation.policy.MetadataPolicy;
import com.nimbusds.openid.connect.sdk.federation.policy.language.PolicyViolationException;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import org.id4me.trustanchor.common.TrustAnchorConfig;
import org.id4me.trustanchor.model.EntityRegistrationForm;
import org.id4me.trustanchor.model.FederationEntity;
import org.id4me.trustanchor.repository.EntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.HashMap;

@Service
public class FederationEntityService {
    private static final Logger logger = LoggerFactory.getLogger(
        FederationEntityService.class
    );
    private final KeyService keyService;
    private final TrustAnchorConfig trustAnchorConfig;
    private final EntityRepository entityRepository;

    public FederationEntityService(
            KeyService keyService,
            TrustAnchorConfig trustAnchorConfig,
            EntityRepository entityRepository
    ) {
        this.keyService = keyService;
        this.trustAnchorConfig = trustAnchorConfig;
        this.entityRepository = entityRepository;
    }

    public String composeSelfSignedStatement() {
        EntityStatementClaimsSet entityStatementClaimsSet = new EntityStatementClaimsSet(
                new EntityID(trustAnchorConfig.getUrl() + ":" + trustAnchorConfig.getPort()),
                new EntityID(trustAnchorConfig.getUrl() + ":" + trustAnchorConfig.getPort()),
                new Date(),
                new Date(),
                keyService.getPublicFedJwkSet()
        );

        //Standard metadata claim
        try {
            entityStatementClaimsSet.setMetadata(
                    FederationMetadataType.FEDERATION_ENTITY,
                    new FederationEntityMetadata(
                            new URI(
                                    trustAnchorConfig.getUrl() +
                                            ":" +
                                            trustAnchorConfig.getPort() +
                                            trustAnchorConfig.getFederationApi()
                            )
                    )
                            .toJSONObject()
            );
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        //Sign Entity Statement with private key from key service. Check kid in key file
        EntityStatement entityStatement = null;
        try {
            entityStatement =
                    EntityStatement.sign(
                            entityStatementClaimsSet,
                            keyService.getFedJwkSet().getKeyByKeyId("JMcm")
                    );
        } catch (JOSEException e) {
            e.printStackTrace();
        }

        assert entityStatement != null;
        return entityStatement.getSignedStatement().serialize();
    }

    public SignedJWT getAndSignStatement(String sub) {
        EntityStatement entityStatement = null;

        //Get stored information about entity
        FederationEntity federationEntity = entityRepository.getFederationEntityBySub(
                sub
        );
        EntityStatementClaimsSet entityStatementClaimsSet = null;
        try {
            entityStatementClaimsSet =
                    new EntityStatementClaimsSet(
                            new EntityID(
                                    trustAnchorConfig.getUrl() + ":" + trustAnchorConfig.getPort()
                            ),
                            new EntityID(federationEntity.getSub()),
                            new Date(),
                            new Date(),
                            JWKSet.parse(federationEntity.getJwkSub())
                    );
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        //Add all type, policy sets to claims set
        EntityStatementClaimsSet finalEntityStatementClaimsSet = entityStatementClaimsSet;
        federationEntity
                .getPolicies()
                .forEach(
                        (type, policy) -> {
                            try {
                                finalEntityStatementClaimsSet.setMetadataPolicy(
                                        new FederationMetadataType(type),
                                        MetadataPolicy.parse(policy)
                                );
                            } catch (ParseException | PolicyViolationException e) {
                                e.printStackTrace();
                            }
                        }
                );

        logger.info(finalEntityStatementClaimsSet.toJSONString());
        //Sign entity statement with issuers private key
        try {
            entityStatement =
                    EntityStatement.sign(
                            entityStatementClaimsSet,
                            keyService.getFedJwkSet().getKeyByKeyId("JMcm")
                    );
        } catch (JOSEException e) {
            e.printStackTrace();
        }

        assert entityStatement != null;
        return entityStatement.getSignedStatement();
    }

    public void saveFederationEntity(EntityRegistrationForm entityRegistrationForm) {
        FederationEntity federationEntity = new FederationEntity();
        federationEntity.setSub(entityRegistrationForm.getSub());
        federationEntity.setIat(new Date());
        federationEntity.setExp(new Date());
        federationEntity.setJwkSub(
                keyService.getPublicJwkSet().toJSONObject().toJSONString()
        );
        @SuppressWarnings("deprecation")
        JSONParser parser = new JSONParser();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject =
                    (JSONObject) parser.parse(entityRegistrationForm.getMetadataPolicy());
        } catch (net.minidev.json.parser.ParseException e) {
            e.printStackTrace();
        }
        logger.info(jsonObject.toJSONString());
        //Map input metadata policy to object
        HashMap<String, JSONObject> policies = new HashMap<>();
        jsonObject.forEach((key, value) -> policies.put(key, (JSONObject) value));
        federationEntity.setPolicies(policies);
        logger.info(federationEntity.getPolicies().toString());

        entityRepository.save(federationEntity);

        try {
            logger.info(
                    getAndSignStatement("http://localhost:8090")
                            .getJWTClaimsSet()
                            .toJSONObject()
                            .toJSONString()
            );
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
    }
}
