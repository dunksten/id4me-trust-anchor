package org.id4me.trustanchor.common;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("trust-anchor")
public class TrustAnchorConfig {
    private String url;
    private int port;
    private String base;
    private String jwks;
    private String federationApi;

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getJwks() {
        return jwks;
    }

    public void setJwks(String jwks) {
        this.jwks = jwks;
    }

    public String getFederationApi() {
        return federationApi;
    }

    public void setFederationApi(String federationApi) {
        this.federationApi = federationApi;
    }
}
