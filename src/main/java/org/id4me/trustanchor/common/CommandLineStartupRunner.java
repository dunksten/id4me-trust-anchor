package org.id4me.trustanchor.common;

import com.nimbusds.openid.connect.sdk.federation.entities.EntityID;
import com.nimbusds.openid.connect.sdk.federation.entities.EntityStatementClaimsSet;
import com.nimbusds.openid.connect.sdk.federation.entities.FederationMetadataType;
import com.nimbusds.openid.connect.sdk.federation.policy.MetadataPolicy;
import com.nimbusds.openid.connect.sdk.federation.policy.operations.ValueOperation;
import org.id4me.trustanchor.service.FederationEntityService;
import org.id4me.trustanchor.service.KeyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class CommandLineStartupRunner implements CommandLineRunner {
    private static final Logger logger = LoggerFactory.getLogger(
        CommandLineStartupRunner.class
    );
    private final KeyService keyService;
    private final FederationEntityService federationEntityService;

    public CommandLineStartupRunner(
            KeyService keyService,
            FederationEntityService federationEntityService
    ) {
        this.keyService = keyService;
        this.federationEntityService = federationEntityService;
    }

    @Override
    public void run(String... args) {
        EntityStatementClaimsSet entityStatementClaimsSet = new EntityStatementClaimsSet(
                new EntityID("http://test"),
                new EntityID("http://test"),
                new Date(),
                new Date(),
                keyService.getPublicJwkSet()
        );
        MetadataPolicy metadataPolicy = new MetadataPolicy();
        ValueOperation valueOperation = new ValueOperation();
        valueOperation.configure("2");
        metadataPolicy.put("tl", valueOperation);

        entityStatementClaimsSet.setMetadataPolicy(
                new FederationMetadataType("test"),
                metadataPolicy
        );
        entityStatementClaimsSet.setMetadataPolicy(
                new FederationMetadataType("test2"),
                metadataPolicy
        );
        logger.info(entityStatementClaimsSet.toJSONString());
    }
}
