package org.id4me.trustanchor.controller;

import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.openid.connect.sdk.SubjectType;
import com.nimbusds.openid.connect.sdk.op.OIDCProviderMetadata;
import net.minidev.json.JSONObject;
import org.id4me.trustanchor.common.TrustAnchorConfig;
import org.id4me.trustanchor.service.FederationEntityService;
import org.id4me.trustanchor.service.KeyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;

@RestController
public class TrustAnchorController {
    private static final Logger logger = LoggerFactory.getLogger(
        TrustAnchorController.class
    );

    private final FederationEntityService federationEntityService;
    private final KeyService keyService;
    private final TrustAnchorConfig trustAnchorConfig;

    public TrustAnchorController(
            FederationEntityService federationEntityService,
            KeyService keyService,
            TrustAnchorConfig trustAnchorConfig
    ) {
        this.federationEntityService = federationEntityService;
        this.keyService = keyService;
        this.trustAnchorConfig = trustAnchorConfig;
    }

    @GetMapping(
            value = "/federation_api_endpoint",
            produces = "application/jose; charset=UTF-8"
    )
    public ResponseEntity<String> apiResponse(
            @RequestParam(value = "iss") String iss,
            @RequestParam(value = "sub") String sub
    ) {
        return ResponseEntity
                .ok()
                .header("Content-Type", "application/jose; charset=UTF-8")
                .body(federationEntityService.getAndSignStatement(sub).serialize());
    }

    @GetMapping(
            value = "/.well-known/jwks.json",
            produces = "application/json; charset=UTF-8"
    )
    public ResponseEntity<String> jwkResponse() {
        return ResponseEntity
                .ok()
                .header("Content-Type", "application/json; charset=UTF-8")
                .body(keyService.getPublicJwkSet().toJSONObject().toJSONString());
    }

    @GetMapping(
            value = "/.well-known/openid-federation",
            produces = "application/jose; charset=UTF-8"
    )
    public ResponseEntity<String> federationConfiguration() {
        return ResponseEntity
                .ok()
                .header("Content-Type", "application/jose; charset=UTF-8")
                .body(federationEntityService.composeSelfSignedStatement());
    }

    @GetMapping("/.well-known/openid-configuration")
    public JSONObject oidcProviderMetadata() {
        OIDCProviderMetadata oidcProviderMetadata = null;
        try {
            oidcProviderMetadata =
                    new OIDCProviderMetadata(
                            new Issuer(
                                    trustAnchorConfig.getUrl() + ":" + trustAnchorConfig.getPort()
                            ),
                            Collections.singletonList(SubjectType.PUBLIC),
                            new URI(
                                    trustAnchorConfig.getUrl() +
                                            ":" +
                                            trustAnchorConfig.getPort() +
                                            trustAnchorConfig.getBase() +
                                            trustAnchorConfig.getJwks()
                            )
                    );
        } catch (URISyntaxException e) {
            logger.error(e.getMessage());
        }
        assert oidcProviderMetadata != null;

        return oidcProviderMetadata.toJSONObject();
    }
}
