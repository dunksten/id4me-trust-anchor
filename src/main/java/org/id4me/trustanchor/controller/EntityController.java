package org.id4me.trustanchor.controller;

import org.id4me.trustanchor.model.EntityRegistrationForm;
import org.id4me.trustanchor.model.FederationEntity;
import org.id4me.trustanchor.repository.EntityRepository;
import org.id4me.trustanchor.service.FederationEntityService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class EntityController {
    private final EntityRepository entityRepository;
    private final FederationEntityService federationEntityService;

    public EntityController(
            EntityRepository entityRepository,
            FederationEntityService federationEntityService
    ) {
        this.entityRepository = entityRepository;
        this.federationEntityService = federationEntityService;
    }

    @GetMapping("/")
    public String home() {
        return "redirect:/index";
    }

    @GetMapping("/index")
    public String index(Model model) {
        model.addAttribute("entities", entityRepository.findAll());
        return "index";
    }

    @GetMapping(value = "/add")
    public String displayForm(
            EntityRegistrationForm entityRegistrationForm,
            Model model
    ) {
        model.addAttribute("entity", entityRegistrationForm);
        return "addEntity";
    }

    @PostMapping(value = "/addEntity")
    public String addEntity(
            EntityRegistrationForm entityRegistrationForm,
            BindingResult result,
            Model model
    ) {
        if (result.hasErrors()) {
            return "addEntity";
        }
        federationEntityService.saveFederationEntity(entityRegistrationForm);
        model.addAttribute("entities", entityRepository.findAll());
        return "redirect:/index";
    }

    @PostMapping(value = "/updateEntity/{id}")
    public String updateEntity(
            @PathVariable("id") Long id,
            FederationEntity federationEntity,
            BindingResult result,
            Model model
    ) {
        if (result.hasErrors()) {
            federationEntity.setId(id);
            return "updateEntity";
        }
        entityRepository.save(federationEntity);
        model.addAttribute("entities", entityRepository.findAll());
        return "redirect:/index";
    }

    @GetMapping("/editEntity/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        FederationEntity federationEntity = entityRepository
                .findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid entity Id:" + id));

        model.addAttribute("entity", federationEntity);
        return "updateEntity";
    }

    @GetMapping("/deleteEntity/{id}")
    public String deleteEntity(@PathVariable("id") long id, Model model) {
        FederationEntity federationEntity = entityRepository
                .findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid entity Id:" + id));
        entityRepository.delete(federationEntity);
        model.addAttribute("entities", entityRepository.findAll());
        return "redirect:/index";
    }
}
