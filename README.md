### Trust anchor for ID4me Trust Framework and Verified Identity

This is the trust anchor for the new ID4me extension Trust Framework.

The other components are:
- https://gitlab.com/dunksten/new-id4me-client-prototype
- https://gitlab.com/dunksten/id4me-intermediate
- https://gitlab.com/dunksten/connect2id-docker

### This component is currently for testing purposes only.
It only functions with specific settings and the other components from the proof-of-concept.
Connect2id server must be running.
Intermediate must be running.

#### Build the project

- Make sure that JDK 8 is installed.
- Open a terminal in the base directory of the project.
- Type ```./mvnw clean install -DskipTests```

#### Run the project

- Type ```./mvnw spring-boot:run```

#### The service now runs at http://localhost:8080